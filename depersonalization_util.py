import logging
from itertools import islice

import phonenumbers

import password_util

SEPARATOR = ','
COLUMN_INDEX = -1
BATCH_SIZE = 1000
FILE = 'data/depersonalized.csv'
RECOVER_FILE = 'data/recovered.csv'


def has_digit(s: str):
    for c in s:
        if c.isdigit():
            return True

    return False


def get_column(row):
    return row.strip().split(SEPARATOR)[COLUMN_INDEX]


def change_column(row, new_value):
    row = row.strip().split(SEPARATOR)
    row[COLUMN_INDEX] = new_value

    return SEPARATOR.join(row)


def write_row(file, row):
    with open(file, 'a') as f:
        f.write(row.strip())
        f.write('\n')


def process(data, mapping: dict):
    preferences = password_util.Preferences(8, 16, True, True, True)
    for i in range(len(data)):
        phone = get_column(data[i])

        if phone not in mapping:
            pwd = password_util.generate_password(preferences)
            mapping[phone] = pwd
            data[i] = change_column(data[i], pwd)
        else:
            data[i] = change_column(data[i], mapping[phone])

        write_row(FILE, data[i])


def clear_files_contents(file):
    with open(file, 'w'):
        pass


def depersonalize(file: str, column: str, mapping: dict):
    global COLUMN_INDEX

    COLUMN_INDEX = -1

    try:
        f = open(file, 'r')
        f.close()
    except FileNotFoundError:
        logging.error(f'Error occurred while opening file: {file}.')
        return

    with open(file, 'r') as f:
        columns = f.readline().strip()
        if columns == '':
            return

        first = f.readline().strip()

    if first == '':
        return

    columns = columns.strip().split(SEPARATOR)
    for i in range(len(columns)):
        if columns[i] == column:
            COLUMN_INDEX = i

    if COLUMN_INDEX == -1:
        logging.error(f'Column "{column}" was not found.')
        return

    is_phone = True
    is_address = False
    is_name = False

    # Trying to check whether given column is column of phone numbers
    try:
        _ = phonenumbers.parse(get_column(first), None)
        is_phone = True
    except phonenumbers.phonenumberutil.NumberParseException:
        try:
            phone = phonenumbers.parse('+' + get_column(first), None)
            if not phonenumbers.is_valid_number(phone):
                is_phone = False
        except phonenumbers.phonenumberutil.NumberParseException:
            is_phone = False

    if not is_phone and has_digit(first):
        is_address = True
    elif not is_phone:
        is_name = True

    clear_files_contents(FILE)
    write_row(FILE, SEPARATOR.join(columns))

    first_batch = True
    with open(file, 'r') as f:
        for n_lines in iter(lambda: tuple(islice(f, BATCH_SIZE)), ()):
            batch = list(n_lines[::])
            if first_batch:
                batch = batch[1:]
                first_batch = False

            if is_phone:
                for i in range(len(batch)):
                    field = get_column(batch[i])
                    field = field.replace(' ', '').replace('-', '').replace('(', '').replace(')', '')
                    field = str(phonenumbers.format_number(phonenumbers.parse(field, None),
                                                           phonenumbers.PhoneNumberFormat.INTERNATIONAL))
                    batch[i] = change_column(batch[i], field)

            process(batch, mapping)

    if is_phone:
        return "phone"
    elif is_name:
        return "name"
    elif is_address:
        return "address"


def recover(column: str, mapping: dict):
    global COLUMN_INDEX

    COLUMN_INDEX = -1

    with open(FILE, 'r') as f:
        columns = f.readline().strip()
        if columns == '':
            return

    columns = columns.strip().split(SEPARATOR)
    for i in range(len(columns)):
        if columns[i] == column:
            COLUMN_INDEX = i

    if COLUMN_INDEX == -1:
        logging.error(f'Column "{column}" was not found.')
        return

    recover_map = {}

    for key in mapping:
        recover_map[mapping[key]] = key

    clear_files_contents(RECOVER_FILE)

    first_batch = True
    with open(FILE, 'r') as f:
        for n_lines in iter(lambda: tuple(islice(f, BATCH_SIZE)), ()):
            batch = list(n_lines[::])
            if first_batch:
                write_row(RECOVER_FILE, batch[0])
                batch = batch[1:]
                first_batch = False

            for i in range(len(batch)):
                field = get_column(batch[i])

                if field not in recover_map:
                    logging.error(f'Error occurred while recovering initial file.')
                    return

                field = recover_map[field]
                batch[i] = change_column(batch[i], field)

                write_row(RECOVER_FILE, batch[i])
