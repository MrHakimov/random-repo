import depersonalization_util
import sys


def print_help():
    print("Incorrect usage of an application. Usage:\n"
          "\t- python3 main.py --file <file name/path> --column <column name>\n")
    exit(0)


args = sys.argv[1:]
if len(args) != 4:
    print_help()

for i in range(len(args), 2):
    if args[i] not in ['--file', '--column']:
        print_help()

file, column = None, None

for i in range(1, len(args), 2):
    if args[i - 1] == '--file':
        file = args[i]
    else:
        column = args[i]

if not file or not column:
    print_help()

mp = {}
column_type = depersonalization_util.depersonalize(file, column, mp)
print('=' * 15)
print(f'Column {column} of the file {file} was depersonalized. The type of the column is "{column_type.upper()}".')
print("Result of depersonalization is stored in 'data/depersonalized.csv'.")

# You can also run recover function to recover initial data. The result would be stored at 'data/recovered.csv'.
# depersonalization_util.recover(column, mp)
